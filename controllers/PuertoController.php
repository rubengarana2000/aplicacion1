<?php

namespace app\controllers;

use Yii;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * PuertoController implements the CRUD actions for Puerto model.
 */
class PuertoController extends Controller
{
    public function actionConsulta9(){
        
       $dataProvider= new ActiveDataProvider([
           'query'=> Puerto::find()
               ->select("nompuerto")
               ->where("altura>1500")
               ->distinct(),
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"consulta 9 con  Active Record",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
           "sql"=>"SELECT nompuerto FROM puerto  altura>1500 "
       ]);
       
    }
    public function actionConsulta9b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT nompuerto FROM puerto where altura>1500',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"consulta 9 con  DAO",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
           "sql"=>"SELECT nompuerto FROM puerto where altura>1500;"
       ]);
    
}
    public function actionConsulta10(){
        
       $dataProvider= new ActiveDataProvider([
           'query'=> Puerto::find()
               ->select("dorsal")
               ->where("pendiente>8 or altura between 1800 and 3000")
               ->distinct(),
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 10 con  Active Record",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
           "sql"=>"SELECT p.dorsal FROM puerto p WHERE p.pendiente>8 or p.altura BETWEEN 1800 AND 3000; "
       ]);
       
    }
     public function actionConsulta10b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT distinct dorsal FROM puerto  WHERE pendiente>8 or altura BETWEEN 1800 AND 3000',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 10 con  DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
           "sql"=>"SELECT distinct dorsal FROM puerto  WHERE pendiente>8 or altura BETWEEN 1800 AND 3000; "
       ]);
    
}
     public function actionConsulta11(){
        
       $dataProvider= new ActiveDataProvider([
           'query'=> Puerto::find()
               ->select("dorsal")
               ->where("pendiente>1500 and altura between 1800 and 3000")
               ->distinct(),
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 11 con  Active Record",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
           "sql"=>"SELECT p.dorsal FROM puerto p WHERE p.pendiente>8 or p.altura BETWEEN 1800 AND 3000; "
       ]);
       
    }
     public function actionConsulta11b(){
        
      $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT distinct dorsal FROM puerto  WHERE pendiente>8 and altura BETWEEN 1800 AND 3000',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 11 con  DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
           "sql"=>"SELECT dorsal FROM puerto  WHERE pendiente>8 and altura BETWEEN 1800 AND 3000;"
       ]);
       
    }
       
       
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Puerto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Puerto model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Puerto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Puerto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nompuerto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Puerto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nompuerto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Puerto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Puerto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Puerto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Puerto::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
