<?php

namespace app\controllers;

use Yii;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;


/**
 * CiclistaController implements the CRUD actions for Ciclista model.
 */
class CiclistaController extends Controller
{
    public function actionConsulta1(){
        
       $dataProvider= new ActiveDataProvider([
           'query'=> Ciclista::find()->select("edad")->distinct(),
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 1 con  Active Record",
           "enunciado"=>"listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclistas"
       ]);
       
       
       
        
    }

    public function actionConsulta2(){
        
       $dataProvider= new ActiveDataProvider([
           'query'=> Ciclista::find()
               ->select("edad")
               ->where("nomequipo = 'Artiach'")
               ->distinct(),
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 2 con  Active Record",
           "enunciado"=>"listar las edades de los ciclistas de artiach ",
           "sql"=>"SELECT DISTINCT edad FROM ciclistas WHERE nomequipo=Artiach"
       ]);
       
       
       
        
    }
    public function actionConsulta3(){
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->where("nomequipo = 'Artiach' or nomequipo= 'Amore Vita' ")
                ->distinct(),
            'pagination'=>[
                'pagesize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclistas WHERE nomequipo=Artiach or nomequipo=Amore Vita"
        ]);
    }
    
    public function actionConsulta4() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("dorsal,edad")
                ->where("edad<25 or edad>30"),
            'pagination'=>[
                'pagesize'=>10,
            ]
            
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal,edad FROM ciclistas WHERE edad<25 or edad>30"
        ]);
    }

    public function actionConsulta5(){
        $dataProvider=new ActiveDataProvider([
           'query'=> Ciclista::find()
                ->select("dorsal,nomequipo,edad")
                ->where("edad between 28 and 32 and nomequipo='Banesto'"),
            
            'pagination'=>[
                'pagesize'=>10,
            ]
                
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nomequipo','edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclistas WHERE edad between 28 and 32 and nomequipo='Banesto'"
        ]);
        
    }
    
    public function actionConsulta6() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre")
                ->where("LENGTH(nombre)>8"),
            'pagination'=>[
                'pagesize'=>10,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre  FROM ciclista WHERE LENGTH(nombre)>8;"
        ]);
        
    }
    public function actionConsulta7() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("upper(nombre) nombre,dorsal"),
                        'pagination'=>[
                'pagesize'=>10,
            ]
               
        
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT Upper(nombre) as mayusculas,dorsal  FROM ciclista ;"
        ]);
        
    }
    public function actionConsulta1b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 1 con  DAO",
           "enunciado"=>" listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclista"
       ]);
    
}
    public function actionConsulta2b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach"',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 2 con  DAO",
           "enunciado"=>"listar las edades de los ciclistas de Artiach",
           "sql"=>"SELECT DISTINCT edad FROM ciclistas WHERE nomequipo=Artiach"
       ]);
    
}
    public function actionConsulta3b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" or nomequipo="Amore Vita"',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 3 con  DAO",
           "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo=Artiach or nomequipo=Amore Vita"
       ]);
    
}
    public function actionConsulta4b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'select dorsal from ciclista where edad<25 or edad>30',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 4 con  DAO",
           "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
           "sql"=>"select dorsal from ciclista where edad<25 or edad>30"
       ]);
    
}
    public function actionConsulta5b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'select dorsal from ciclista where edad between 28 and 32 and nomequipo="Banesto"',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 5 con  DAO",
           "enunciado"=>" listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>"select dorsal from ciclista where edad between 28 and 32 and nomequipo=Banesto"
       ]);
    
}
    public function actionConsulta6b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'select nombre from ciclista where length(nombre)>8',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"consulta 6 con  DAO",
           "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>"select nombre from ciclista where length(nombre)>8"
       ]);
    
}
public function actionConsulta7b(){
   
    
    $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT Upper(nombre) nombre,dorsal FROM ciclista',
           
           'pagination'=>[
               'pagesize'=>10,
           ]
           
           
       ]);
        
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre','dorsal'],
           "titulo"=>"consulta 7 con  DAO",
           "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
           "sql"=>"SELECT Upper(nombre) nombre,dorsal FROM ciclista ;"
       ]);
    
}

    

    



    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ciclista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ciclista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ciclista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ciclista();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dorsal]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ciclista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dorsal]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ciclista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    
    

    /**
     * Finds the Ciclista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ciclista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ciclista::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
