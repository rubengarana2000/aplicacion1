<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas 1';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1> Ejercicios Boletín Cosultas 1</h1>

    </div>
    <div class="body-content">

        <div class="row">
            <!-- boton de la consulta-->
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p> listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta1'], ['class' => 'btn btn-primary']) ?>                            
<?= Html :: a('DAO', ['ciclista/consulta1b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            
            
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p> listar las edades de los ciclistas de Artiach</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta2'], ['class' => 'btn btn-primary']) ?>                            
<?= Html :: a(' DAO', ['ciclista/consulta2b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p> listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta3'], ['class' => 'btn btn-primary']) ?>                            
<?= Html :: a('DAO', ['ciclista/consulta3b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            </div>
        <div class="row">
        <div class="row">
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p> listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta4'], ['class' => 'btn btn-primary']) ?>                           
<?= Html :: a('DAO', ['ciclista/consulta4b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>  listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta5'], ['class' => 'btn btn-primary']) ?>                           
<?= Html :: a('DAO', ['ciclista/consulta5b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>  Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta6'], ['class' => 'btn btn-primary']) ?>                            
<?= Html :: a('DAO', ['ciclista/consulta6b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>
            </div>
</div>
        <div class="row">
        
            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>  lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
<?= Html :: a('Active Record', ['ciclista/consulta7'], ['class' => 'btn btn-primary']) ?>                            
<?= Html :: a('DAO', ['ciclista/consulta7b'], ['class' => 'btn btn-danger']) ?>                        
                        </p>

                    </div>
                </div>


            </div>



        
        <div class="col-sm-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p> Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                    <p>
<?= Html :: a('Active Record', ['lleva/consulta8'], ['class' => 'btn btn-primary']) ?>                       
<?= Html :: a('DAO', ['lleva/consulta8b'], ['class' => 'btn btn-danger']) ?>                    
                    </p>

                </div>
            </div>


        </div>
        <div class="col-sm-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p> Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                    <p>
<?= Html :: a('Active Record', ['puerto/consulta9'], ['class' => 'btn btn-primary']) ?>                        
<?= Html :: a('DAO', ['puerto/consulta9b'], ['class' => 'btn btn-danger']) ?>                   
                    </p>

                </div>
            </div>


        </div>
            </div>
        <div class="row">
        <div class="col-sm-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p> Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 </p>
                    <p>
<?= Html :: a('Active Record', ['puerto/consulta10'], ['class' => 'btn btn-primary']) ?>                       
<?= Html :: a('DAO', ['puerto/consulta10b'], ['class' => 'btn btn-danger']) ?>                        
                    </p>

                </div>
            </div>


        </div>

            <div class="col-sm-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                    <p>  Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                    <p>
                   
 <?= Html :: a('Active Record', ['puerto/consulta11'], ['class' => 'btn btn-primary']) ?>                                  
<?= Html :: a('DAO', ['puerto/consulta11'], ['class' => 'btn btn-danger']) ?>
                    </p>
                  

                </div>
            </div>


        </div>
            



    </div>



        
        
        
        
        
    </div>
    
    
    
</div>

